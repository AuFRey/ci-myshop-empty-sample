package com.ci.myShop.controller;

import java.util.*;

import com.ci.myShop.model.Item;

public class Storage {
    Map<String , Item> itemMap;

    public Storage() {
        this.itemMap = new HashMap<String, Item>();
    }

    public void addItem(Item obj) {
        itemMap.put(obj.getName(), obj);
    }

    public Item getItem(String name) {
        return itemMap.get(name);
    }
}
