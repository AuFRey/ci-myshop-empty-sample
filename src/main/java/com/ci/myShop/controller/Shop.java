package com.ci.myShop.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;

public class Shop {

	float cash = 10;
	Storage storage;

	public Shop(Storage storage) {
		super();
		this.storage = storage;
	}

	public boolean isItemAvailable(String name) {
		
		if (storage.getItem(name) == null) {
			System.out.println("Cet article n'est pas disponible dans le shop");
			return false;
		}
		
		return true;
	}
	
	public boolean buy(Item item) {
		
		if (!this.isItemAvailable(item.getName())) {
			return false;
		}
		
		if (item.getPrice() > cash) {
			System.out.println("Vous n'avez pas assez d'argent");
			return false; 
		}
		
		cash = cash - item.getPrice();
		return true; 
			
		}

    public Item sell(String name) {
        Item item = storage.getItem(name);
        cash += item.getPrice();
		return item;
	}

    public float getCash() {
        return cash;
    }
    
    public int getAgeForBook(String name) {
    	// r�cup�rer l'item � partir du name
    	Item item = storage.getItem(name);
    	
    	// v�rifier si l'item est un livre
    	if (item instanceof Book) {
    		return ((Book) item).getAge();
    	}
    	
    	return 0;    	
    }
    
    public List<Book>getAllBook() {
    	// Cr�er une liste vide de Books
    	List<Book> books = new ArrayList<Book>();
    	
    	// it�rer chaque item du shop
    	for (Item item : storage.itemMap.values()) {
    		// l'ajouter � la liste si c'est un Book
    		if (item instanceof Book) {
    			books.add((Book) item);
    		}

    	}
    	return books;
    }
    	
    
    public int getNbItemInStorage(String name) {
    	
    	if (!this.isItemAvailable(name)) {
			return 0;
		}
    	
    	Item item = storage.getItem(name);
    	return item.getNbrElt();
    }
    
    public int getQuantityPerConsumable(String name) {
    
    	Item item = storage.getItem(name);
    	
    	if (item instanceof Consumable) {
    		return ((Consumable) item).getQuantity();
    	}
    	
    	return 1;
    }
}
