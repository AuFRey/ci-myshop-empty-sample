package com.ci.myShop.model;

import java.util.List;

public class MusicalBook extends Book {

	List<String>listOfSound;
	int lifetime;
	int nbrBattery;
	
	public List<String> getListOfSound() {
		return listOfSound;
	}
	public void setListOfSound(List<String> listOfSound) {
		this.listOfSound = listOfSound;
	}
	public int getLifetime() {
		return lifetime;
	}
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}
	public int getNbrBattery() {
		return nbrBattery;
	}
	public void setNbrBattery(int nbrBattery) {
		this.nbrBattery = nbrBattery;
	}
	
	
}
