package com.ci.myShop.model;

public class Item {

	String name;
	int id;
	float price;
	int nbrElt;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}

	public String display() {
		return name + " " + price ;
	}

}
