package com.ci.myShop.starter;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Book;
import com.ci.myShop.model.Item;


public class Launcher {
	public static void main(String[] args) {

        Storage storage = new Storage();
       

	    // Créer deux items
        Item item1 = new Item();
        item1.setName("article 1");
        item1.setPrice(10);
        storage.addItem(item1);

        Item item2 = new Item();
        item2.setName("article 2");
        item2.setPrice(5);
        storage.addItem(item2);
        
        // Cr�er un livre
        Book book = new Book();
        book.setAge(7);
        book.setName("Voyage au centre de la Terre");
        book.setAuthor("Jules Verne");
        // associer le livre au Shop
        storage.addItem(book);
        
        // Afficher l'age minimal pour ce livre
        Shop shop = new Shop(storage);
        
        System.out.println(shop.getAllBook());
        
        System.out.println("l'�ge minimum pour ce livre est de : " + shop.getAgeForBook(book.getName()));
        
        // Essayer d'afficher l'age minimal pour un autre item (pas un livre)
        System.out.println("l'�ge minimum pour ce livre est de : " + shop.getAgeForBook(item1.getName()));        
		  
		// vendre un article 
        shop.sell(item1.getName());
		  
		// Afficher le cash du shop 
        System.out.println("Vous avez : " + shop.getCash());
		  
		  // Acheter un article 
        shop.buy(item2);
		  
		  // Afficher le cash du shop 
        System.out.println("Il vous reste : " + shop.getCash());
		 
	}

}
