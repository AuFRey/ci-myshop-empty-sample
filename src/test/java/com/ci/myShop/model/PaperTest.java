package com.ci.myShop.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PaperTest {
    protected Paper paper;

    @Before
    public void setUp() {
        paper = new Paper();
    }

    @Test
    public void qualityTest() {
        paper.setQuality("premium");
        assertEquals("premium", paper.getQuality());
    }

    @Test
    public void weightTest() {
        float weight = 400F;
        paper.setWeight(weight);
        assertTrue(weight == paper.getWeight());
    }
}
