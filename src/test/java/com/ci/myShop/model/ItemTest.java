package com.ci.myShop.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertEquals;

public class ItemTest {
    @Test
    public void testItemName(){
        Item item = new Item ();
        item.setName("testItem");
        //assertEquals permet de vérifier que les deux paramètres renseignés sont égaux pour que le test soit validé
        assertEquals("testItem",item.getName());
    }

    @Test
    public void testItemPrice() {
        Item item = new Item();
        item.setPrice((float) 99.99);
        //assertEquals permet de vérifier que les deux paramètres renseignés sont égaux pour que le test soit validé
        assertEquals((float) 99.99, item.getPrice());
    }

    @Test
    public void testItemId() {
        Item item = new Item();
        item.setId((int) 4);
        //assertEquals permet de vérifier que les deux paramètres renseignés sont égaux pour que le test soit validé
        assertEquals((int) 4, item.getId());
    }

    @Test
    public void testItemNbrElt() {
        Item item = new Item();
        item.setNbrElt((int) 4);
        //assertEquals permet de vérifier que les deux paramètres renseignés sont égaux pour que le test soit validé
        assertEquals((int) 4, item.getNbrElt());
    }

    @Test
    public void testItemDisplay() {
        Item item = new Item();
        item.setName("Mon item");
        item.setPrice((float) 15.55);
        assertEquals("Mon item 15.55", item.display());
    }

}
