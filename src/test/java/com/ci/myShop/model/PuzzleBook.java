package com.ci.myShop.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class PuzzleBook {

    @Test
    public void testBookDurability() {
        BookToTouch book = new BookToTouch();
        int value = 100;
        book.setDurability(value);
        assertEquals(value, book.getDurability());
    }

    @Test
    public void testBgetMaterial() {
        BookToTouch book = new BookToTouch();
        String value = "Paperback";
        book.setMaterial(value);
        assertEquals(value, book.getMaterial());
    }
}
