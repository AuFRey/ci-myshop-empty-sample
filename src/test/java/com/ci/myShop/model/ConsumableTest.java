package com.ci.myShop.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConsumableTest {

    @Test
    public void consumableQuantityTest() {
        Consumable consumable = new Consumable();
        consumable.setQuantity(100);
        assertEquals(100, consumable.getQuantity());
    }
}
