package com.ci.myShop.model;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Item;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BookTest {

    protected Book book;

    @Before
    public void setUp() {
        book = new Book();
    }

    @Test
    public void testBookNbPage() {
        int value = 100;
        book.setNbPage(value);
        assertEquals(value, book.getNbPage());
    }

    @Test
    public void testBookAuthor() {
        String value = "F. J. Fitzgerald";
        book.setAuthor(value);
        assertEquals(value, book.getAuthor());
    }

    @Test
    public void testBookPublisher() {
        String value = "Penguin Classics";
        book.setPublisher(value);
        assertEquals(value, book.getPublisher());
    }

    @Test
    public void testBookYear() {
        int value = 1999;
        book.setYear(value);
        assertEquals(value, book.getYear());
    }

    @Test
    public void testBookAge() {
        int value = 8;
        book.setAge(value);
        assertEquals(value, book.getAge());
    }

    @Test
    public void testToString() {
        String name = "The Great Gatsby";
        book.setName(name);
        assertEquals("Book [name=" + name  + "]", book.toString());
    }
}
