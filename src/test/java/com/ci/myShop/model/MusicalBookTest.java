package com.ci.myShop.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MusicalBookTest {

    @Test
    public void testMBookSoundList(){

        MusicalBook quietBook = new MusicalBook();
        List<String> sonsNature = new ArrayList<>();

        sonsNature.add ("Beach");
        sonsNature.add ("Mountain");
        sonsNature.add  ("Cabin");

        quietBook.setListOfSound(sonsNature);
        assertEquals(sonsNature,quietBook.getListOfSound());

    }

    @Test
    public void testMBookLifetime(){
        MusicalBook quietBook = new MusicalBook();
        quietBook.setLifetime(5);
        assertEquals(5,quietBook.getLifetime());
    }

    @Test
    public void testMBookNbrBattery(){
        MusicalBook quietBook = new MusicalBook();
        quietBook.setNbrBattery(5);
        assertEquals(5,quietBook.getNbrBattery());
    }

}
