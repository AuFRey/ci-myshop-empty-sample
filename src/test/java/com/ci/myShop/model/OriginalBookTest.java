package com.ci.myShop.model;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class OriginalBookTest {

    protected OriginalBook book;

    @Before
    public void setUp() {
        book = new OriginalBook();
    }

    @Test
    public void testIsNumeric() {
        book.setNumeric(true);
        assertTrue(book.isNumeric());
    }
}
